# Blockchain Vue
This is a small application to represent a Blockchain and it's Blocks, highlighting the hash references. 

I created two Javascript classes, Blockchain.js and Block.js. The Blockchain class has methods to initialize the chain, add Blocks and validate the chain. The Block class uses the crypto/SHA256  methods to create the hash of each Block 

The frontend is built using Vue.js. I've created two separate components, chain.vue and block.vue. As expected, the data of each Block is sent from the chain component to the block component via props.


### Prerequisites

You need Node and NPM installed.


### Installing

To install it in local, clone or download and install all dependencies with:

```
npm install
```
### Run locally
Use Webpack 4 build command to compile all JS classes and components:

```
npm run build
```

Then open index.html with your preffered browser.

## Built With
* Node
* Vue.js
* Webpack 4


## Authors

* **Antonio Ufano** - *Initial work* 
