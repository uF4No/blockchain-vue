
import Block from './Block'

export default class Blockchain{
  constructor(){
    this.chain = [this.createGenesisBlock()];
  }

  createGenesisBlock(){
  //return a Block instance with the attributes hardcoded
    var genesisData = 'Genesis block';
    var genesisPrevHash = "0000000000";
    return new Block(0, genesisData, genesisPrevHash);
  }

  getLatestBlock(){
  //returns the block of the last index of the chain
    return this.chain[this.chain.length - 1];
  }

  addBlock(newBlock){
    //assign index
    newBlock.index = this.chain.length;
    //assign hashes
    newBlock.previousHash = this.getLatestBlock().hash;
    newBlock.hash = newBlock.calculateHash();
    //add to the chain
    this.chain.push(newBlock);

  }

  isValid(){
    for(let i =1; i < this.chain.length; i++){
      const current = this.chain[i];
      const previous = this.chain[i -1];

      if(current.previousHash !== previous.hash){
      //if chain of hashes is not correct
        console.log("Block " + i + " is incorrect! Previous Hash not valid");
        return i;
      }

      if(current.hash !== current.calculateHash()){
      //if hash of a block is not correct
        console.log("Block " + i + " is incorrect! Hash not valid");
        return i;
      }
    }
    //if validates all blocks, ok
    console.log('Validation ok, returning 0...')
    return 0;
  }
}
