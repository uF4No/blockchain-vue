const SHA256 = require("crypto-js/sha256");

export default class Block{
  constructor( index, data, previousHash){

    this.data = data;
    this.timestamp = Date.now();
    this.index = index;
    this.previousHash = previousHash;
    this.hash = this.calculateHash();
  }

  calculateHash(){
    return SHA256(this.index + this.previousHash + this.timestamp + JSON.stringify(this.data)).toString();
  }
}
